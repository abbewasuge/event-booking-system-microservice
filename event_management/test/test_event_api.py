import unittest
import psycopg2

class TestDatabase(unittest.TestCase):
    def setUp(self):
        # Establish a connection to the PostgreSQL database in the Docker container
        self.connection = psycopg2.connect(
            host='database',
            port='5432',
            user='postgres',
            password='password',
            database='shared_db'
        )

    def tearDown(self):
        # Close the database connection after each test
        self.connection.close()

    def test_events_table_exists(self):
        cursor = self.connection.cursor()

        # Execute a query to check if the "events" table exists
        cursor.execute("SELECT EXISTS(SELECT 1 FROM information_schema.tables WHERE table_name = 'events')")

        # Fetch the result of the query
        result = cursor.fetchone()[0]

        # Assert that the result is True, indicating the table exists
        self.assertTrue(result)

        cursor.close()

    def test_insert_event(self):
        cursor = self.connection.cursor()

        # Insert a new event into the "events" table
        cursor.execute("INSERT INTO events (name, date, location) VALUES ('Event 1', '2023-01-01', 'Location 1')")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the last inserted event from the table
        cursor.execute("SELECT name, date, location FROM events ORDER BY id DESC LIMIT 1")
        event_data = cursor.fetchone()

        # Convert the fetched date to string
        event_data = (event_data[0], str(event_data[1]), event_data[2])

        # Assert that the fetched event data matches the inserted values
        self.assertEqual(event_data, ('Event 1', '2023-01-01', 'Location 1'))

        cursor.close()

    def test_get_event(self):
        cursor = self.connection.cursor()

        # Insert a new event into the "events" table
        cursor.execute("INSERT INTO events (name, date, location) VALUES ('Event 2', '2023-02-02', 'Location 2')")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the event ID of the last inserted event
        cursor.execute("SELECT id FROM events ORDER BY id DESC LIMIT 1")
        event_id = cursor.fetchone()[0]

        # Fetch the event details using the event ID
        cursor.execute("SELECT name, date, location FROM events WHERE id = %s", (event_id,))
        event_data = cursor.fetchone()

        # Convert the fetched date to string
        event_data = (event_data[0], str(event_data[1]), event_data[2])

        # Assert that the fetched event data matches the expected values
        self.assertEqual(event_data, ('Event 2', '2023-02-02', 'Location 2'))

        cursor.close()

    def test_delete_event(self):
        cursor = self.connection.cursor()

        # Insert a new event into the "events" table
        cursor.execute("INSERT INTO events (name, date, location) VALUES ('Event 3', '2023-03-03', 'Location 3')")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the event ID of the last inserted event
        cursor.execute("SELECT id FROM events ORDER BY id DESC LIMIT 1")
        event_id = cursor.fetchone()[0]

        # Delete the event from the "events" table
        cursor.execute("DELETE FROM events WHERE id = %s", (event_id,))

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the event details using the event ID
        cursor.execute("SELECT name, date, location FROM events WHERE id = %s", (event_id,))
        event_data = cursor.fetchone()

        # Assert that the fetched event data is None, indicating the event was deleted
        self.assertIsNone(event_data)

        cursor.close()

if __name__ == '__main__':
    unittest.main()

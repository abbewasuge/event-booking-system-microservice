from fastapi import FastAPI, HTTPException
from prometheus_client import Counter, generate_latest
from prometheus_fastapi_instrumentator import Instrumentator
from .connection import get_db_connection, create_events_table
from .models import EventAttendee, Event

create_events_table()

app = FastAPI()

# Create the Prometheus metrics
event_created_total = Counter(
    'event_created_total',
    'Total number of event creations',
)

event_updated_total = Counter(
    'event_updated_total',
    'Total number of event updates',
)

event_deleted_total = Counter(
    'event_deleted_total',
    'Total number of event deletions',
)

http_requests_total = Counter(
    'http_requests_total',
    'Total number of HTTP requests',
    ['method', 'endpoint'],
)

# Instrument the app with Prometheus middleware
Instrumentator().instrument(app).expose(app)

# Routes
@app.get("/")
def read_root():
    http_requests_total.labels(method="GET", endpoint="/").inc()
    return {"message": "Welcome to the Event Management Microservice!"}

@app.post("/events")
def create_event(event: Event):
    try:
        conn = get_db_connection()
        cur = conn.cursor()
        cur.execute(
            "INSERT INTO events (name, date, location) VALUES (%s, %s, %s) RETURNING id",
            (event.name, event.date, event.location),
        )
        event_id = cur.fetchone()[0]
        conn.commit()
        cur.close()
        conn.close()
        event_created_total.inc()  # Increment the event_created_total counter
        http_requests_total.labels(method="POST", endpoint="/events").inc()
        return {"message": "Event created successfully!", "event_id": event_id}
    except Exception as e:
        # Log the error message
        print(f"Error creating event: {str(e)}")
        # Raise an HTTPException with appropriate status code and error message
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/events")
def get_all_events():
    http_requests_total.labels(method="GET", endpoint="/events").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("SELECT id, name, date, location FROM events")
    events = []
    for event_data in cur.fetchall():
        event = {
            "id": event_data[0],
            "name": event_data[1],
            "date": event_data[2],
            "location": event_data[3],
        }
        events.append(event)
    cur.close()
    conn.close()
    return {"events": events}

@app.get("/events/{event_id}")
def get_event(event_id: int):
    http_requests_total.labels(method="GET", endpoint="/events/{event_id}").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "SELECT name, date, location FROM events WHERE id = %s",
        (event_id,),
    )
    event_data = cur.fetchone()
    cur.close()
    conn.close()

    if event_data:
        event = {
            "id": event_id,
            "name": event_data[0],
            "date": event_data[1],
            "location": event_data[2],
        }
        return {"event": event}
    else:
        raise HTTPException(status_code=404, detail="Event not found")

@app.put("/events/{event_id}")
def update_event(event_id: int, event: Event):
    http_requests_total.labels(method="PUT", endpoint="/events/{event_id}").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "UPDATE events SET name = %s, date = %s, location = %s WHERE id = %s",
        (event.name, event.date, event.location, event_id),
    )
    conn.commit()
    cur.close()
    conn.close()
    event_updated_total.inc()  # Increment the event_updated_total counter
    return {"message": "Event updated successfully!"}

@app.delete("/events/{event_id}")
def delete_event(event_id: int):
    http_requests_total.labels(method="DELETE", endpoint="/events/{event_id}").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("DELETE FROM events WHERE id = %s", (event_id,))
    conn.commit()
    cur.close()
    conn.close()
    event_deleted_total.inc()  # Increment the event_deleted_total counter
    return {"message": "Event deleted successfully!"}

@app.get("/events/date_range")
def get_events_within_date_range(start_date: str, end_date: str):
    http_requests_total.labels(method="GET", endpoint="/events/date_range").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "SELECT id, name, date, location FROM events WHERE date >= %s AND date <= %s",
        (start_date, end_date),
    )
    events = []
    for event_data in cur.fetchall():
        event = {
            "id": event_data[0],
            "name": event_data[1],
            "date": event_data[2],
            "location": event_data[3],
        }
        events.append(event)
    cur.close()
    conn.close()
    return {"events": events}

@app.get("/users/{user_id}/events")
def get_user_events(user_id: int):
    http_requests_total.labels(method="GET", endpoint="/users/{user_id}/events").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "SELECT e.id, e.name, e.date, e.location "
        "FROM events e "
        "JOIN event_attendees ea ON e.id = ea.event_id "
        "WHERE ea.user_id = %s",
        (user_id,),
    )
    events = []
    for event_data in cur.fetchall():
        event = {
            "id": event_data[0],
            "name": event_data[1],
            "date": event_data[2],
            "location": event_data[3],
        }
        events.append(event)
    cur.close()
    conn.close()
    return {"events": events}

@app.post("/events/{event_id}/attendees")
def add_event_attendee(event_id: int, attendee: EventAttendee):
    http_requests_total.labels(method="POST", endpoint="/events/{event_id}/attendees").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "INSERT INTO event_attendees (event_id, user_id) VALUES (%s, %s)",
        (event_id, attendee.user_id),
    )
    conn.commit()
    cur.close()
    conn.close()
    return {"message": "Event attendee added successfully!"}

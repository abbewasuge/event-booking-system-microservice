import time
import psycopg2
from retry import retry

# Database connection configuration
DATABASE_URL = "postgresql://postgres:password@database:5432/shared_db"

# Function to create a connection to the database
def get_db_connection():
    return psycopg2.connect(DATABASE_URL)

# Retry decorator to retry the connection
@retry(psycopg2.OperationalError, tries=5, delay=1)
def connect_with_retry():
    return get_db_connection()

# Function to create the "events" table
def create_events_table():
    conn = connect_with_retry()
    cur = conn.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS events (
            id SERIAL PRIMARY KEY,
            name TEXT NOT NULL,
            description TEXT,
            date DATE NOT NULL,
            location TEXT
        )
    """)
    conn.commit()
    cur.close()
    conn.close()

# Call the function to create the table
create_events_table()



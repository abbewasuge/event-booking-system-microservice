from pydantic import BaseModel

class Event(BaseModel):
    name: str
    date: str
    location: str

class EventAttendee(BaseModel):
    event_id: int
    user_id: int

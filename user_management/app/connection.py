import psycopg2

# Database connection configuration
DATABASE_URL = "postgresql://postgres:password@database:5432/shared_db"

# Function to create a connection to the database
def get_db_connection():
    return psycopg2.connect(DATABASE_URL)

def create_users_table():
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS users (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL
        )
    """)
    conn.commit()
    cur.close()
    conn.close()
    

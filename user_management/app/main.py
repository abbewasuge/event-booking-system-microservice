from fastapi import FastAPI, HTTPException
from prometheus_fastapi_instrumentator import Instrumentator
from prometheus_client import Counter
from .connection import get_db_connection, create_users_table
from .models import User

# Create the Prometheus metrics
user_created_total = Counter(
    'user_created_total',
    'Total number of user creations',
)

user_updated_total = Counter(
    'user_updated_total',
    'Total number of user updates',
)

user_deleted_total = Counter(
    'user_deleted_total',
    'Total number of user deletions',
)

http_requests_total = Counter(
    'http_requests_total',
    'Total number of HTTP requests',
    ['method', 'endpoint'],
)

create_users_table()

app = FastAPI()

# Instrument the app with Prometheus middleware
Instrumentator().instrument(app).expose(app)

# Routes
@app.get("/")
async def read_root():
    http_requests_total.labels(method="GET", endpoint="/").inc()
    return {"message": "Welcome to the User Management Microservice!"}

@app.post("/users")
async def create_user(user: User):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        'INSERT INTO users (name, email) VALUES (%s, %s) RETURNING id',
        (user.name, user.email),
    )
    result = cur.fetchone()
    if result is not None and len(result) > 0:
        user_id = result[0]
        conn.commit()
        cur.close()
        conn.close()
        user_created_total.inc()  # Increment the user_created_total counter
        return {"message": "User created successfully!", "user_id": user_id}
    else:
        conn.rollback()
        cur.close()
        conn.close()
        raise HTTPException(status_code=500, detail="Failed to create user")

@app.get("/users")
async def get_all_users():
    http_requests_total.labels(method="GET", endpoint="/users").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("SELECT id, name, email FROM users")
    users = []
    for user_data in cur.fetchall():
        user = {
            "id": user_data[0],
            "name": user_data[1],
            "email": user_data[2],
        }
        users.append(user)
    cur.close()
    conn.close()
    return {"users": users}

@app.get("/users/{user_id}")
async def get_user(user_id: int):
    http_requests_total.labels(method="GET", endpoint="/users/{user_id}").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "SELECT name, email FROM users WHERE id = %s",
        (user_id,),
    )
    user_data = cur.fetchone()
    cur.close()
    conn.close()

    if user_data:
        user = {
            "id": user_id,
            "name": user_data[0],
            "email": user_data[1],
        }
        return {"user": user}
    else:
        raise HTTPException(status_code=404, detail="User not found")

@app.put("/users/{user_id}")
async def update_user(user_id: int, user: User):
    http_requests_total.labels(method="PUT", endpoint="/users/{user_id}").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "UPDATE users SET name = %s, email = %s WHERE id = %s",
        (user.name, user.email, user_id),
    )
    conn.commit()
    cur.close()
    conn.close()
    user_updated_total.inc()
    return {"message": "User updated successfully!"}

@app.delete("/users/{user_id}")
async def delete_user(user_id: int):
    http_requests_total.labels(method="DELETE", endpoint="/users/{user_id}").inc()
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("DELETE FROM users WHERE id = %s", (user_id,))
    conn.commit()
    cur.close()
    conn.close()
    user_deleted_total.inc()
    return {"message": "User deleted successfully!"}

fastapi
pydantic
psycopg2-binary
uvicorn
retry==0.9.2
prometheus_client==0.11.0
prometheus-fastapi-instrumentator
import unittest
import psycopg2

class TestUserDatabase(unittest.TestCase):
    def setUp(self):
        # Establish a connection to the PostgreSQL database in the Docker container
        self.connection = psycopg2.connect(
            host='database',
            port='5432',
            user='postgres',
            password='password',
            database='shared_db'
        )

    def tearDown(self):
        # Close the database connection after each test
        self.connection.close()

    def test_users_table_exists(self):
        cursor = self.connection.cursor()

        # Execute a query to check if the "users" table exists
        cursor.execute("SELECT EXISTS(SELECT 1 FROM information_schema.tables WHERE table_name = 'users')")

        # Fetch the result of the query
        result = cursor.fetchone()[0]

        # Assert that the result is True, indicating the table exists
        self.assertTrue(result)

        cursor.close()

    def test_insert_user(self):
        cursor = self.connection.cursor()

        # Insert a new user into the "users" table
        cursor.execute("INSERT INTO users (name, email) VALUES ('John Doe', 'john.doe@example.com')")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the last inserted user from the table
        cursor.execute("SELECT name, email FROM users ORDER BY id DESC LIMIT 1")
        user_data = cursor.fetchone()

        # Assert that the fetched user data matches the inserted values
        self.assertEqual(user_data, ('John Doe', 'john.doe@example.com'))

        cursor.close()

    def test_get_user(self):
        cursor = self.connection.cursor()

        # Insert a new user into the "users" table
        cursor.execute("INSERT INTO users (name, email) VALUES ('Jane Smith', 'jane.smith@example.com')")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the user ID of the last inserted user
        cursor.execute("SELECT id FROM users ORDER BY id DESC LIMIT 1")
        user_id = cursor.fetchone()[0]

        # Fetch the user details using the user ID
        cursor.execute("SELECT name, email FROM users WHERE id = %s", (user_id,))
        user_data = cursor.fetchone()

        # Assert that the fetched user data matches the expected values
        self.assertEqual(user_data, ('Jane Smith', 'jane.smith@example.com'))

        cursor.close()

    def test_delete_user(self):
        cursor = self.connection.cursor()

        # Insert a new user into the "users" table
        cursor.execute("INSERT INTO users (name, email) VALUES ('Alex Johnson', 'alex.johnson@example.com')")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the user ID of the last inserted user
        cursor.execute("SELECT id FROM users ORDER BY id DESC LIMIT 1")
        user_id = cursor.fetchone()[0]

        # Delete the user from the "users" table
        cursor.execute("DELETE FROM users WHERE id = %s", (user_id,))

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the user details using the user ID
        cursor.execute("SELECT name, email FROM users WHERE id = %s", (user_id,))
        user_data = cursor.fetchone()

        # Assert that the fetched user data is None, indicating the user was deleted
        self.assertIsNone(user_data)

        cursor.close()

if __name__ == '__main__':
    unittest.main()

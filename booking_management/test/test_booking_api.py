import unittest
import psycopg2

class TestDatabase(unittest.TestCase):
    def setUp(self):
        # Establish a connection to the PostgreSQL database in the Docker container
        self.connection = psycopg2.connect(
            host='database',
            port='5432',
            user='postgres',
            password='password',
            database='shared_db'
        )

    def tearDown(self):
        # Close the database connection after each test
        self.connection.close()

    def test_bookings_table_exists(self):
        cursor = self.connection.cursor()

        # Execute a query to check if the "bookings" table exists
        cursor.execute("SELECT EXISTS(SELECT 1 FROM information_schema.tables WHERE table_name = 'bookings')")

        # Fetch the result of the query
        result = cursor.fetchone()[0]

        # Assert that the result is True, indicating the table exists
        self.assertTrue(result)

        cursor.close()

    def test_insert_booking(self):
        cursor = self.connection.cursor()

        # Insert a new booking into the "bookings" table
        cursor.execute("INSERT INTO bookings (user_id, event_id) VALUES (1, 1)")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the last inserted booking from the table
        cursor.execute("SELECT user_id, event_id FROM bookings ORDER BY id DESC LIMIT 1")
        booking_data = cursor.fetchone()

        # Assert that the fetched booking data matches the inserted values
        self.assertEqual(booking_data, (1, 1))

        cursor.close()

    def test_get_booking(self):
        cursor = self.connection.cursor()

        # Insert a new booking into the "bookings" table
        cursor.execute("INSERT INTO bookings (user_id, event_id) VALUES (2, 2)")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the booking ID of the last inserted booking
        cursor.execute("SELECT id FROM bookings ORDER BY id DESC LIMIT 1")
        booking_id = cursor.fetchone()[0]

        # Fetch the booking details using the booking ID
        cursor.execute("SELECT user_id, event_id FROM bookings WHERE id = %s", (booking_id,))
        booking_data = cursor.fetchone()

        # Assert that the fetched booking data matches the expected values
        self.assertEqual(booking_data, (2, 2))

        cursor.close()

    def test_delete_booking(self):
        cursor = self.connection.cursor()

        # Insert a new booking into the "bookings" table
        cursor.execute("INSERT INTO bookings (user_id, event_id) VALUES (3, 3)")

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the booking ID of the last inserted booking
        cursor.execute("SELECT id FROM bookings ORDER BY id DESC LIMIT 1")
        booking_id = cursor.fetchone()[0]

        # Delete the booking from the "bookings" table
        cursor.execute("DELETE FROM bookings WHERE id = %s", (booking_id,))

        # Commit the transaction to persist the changes
        self.connection.commit()

        # Fetch the booking details using the booking ID
        cursor.execute("SELECT user_id, event_id FROM bookings WHERE id = %s", (booking_id,))
        booking_data = cursor.fetchone()

        # Assert that the fetched booking data is None, indicating the booking was deleted
        self.assertIsNone(booking_data)

        cursor.close()

if __name__ == '__main__':
    unittest.main()

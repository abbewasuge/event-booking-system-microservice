import psycopg2

# Database connection configuration
DATABASE_URL = "postgresql://postgres:password@database:5432/shared_db"

# Function to create a connection to the database
def get_db_connection():
    return psycopg2.connect(DATABASE_URL)

def create_booking_table():
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS bookings (
            id SERIAL PRIMARY KEY,
            user_id INT NOT NULL,
            event_id INT NOT NULL,
            FOREIGN KEY (user_id) REFERENCES users(id),
            FOREIGN KEY (event_id) REFERENCES events(id)
        )
    """)
    conn.commit()
    cur.close()
    conn.close()




from fastapi import HTTPException, FastAPI
from pydantic import BaseModel
from prometheus_client import Counter
from prometheus_fastapi_instrumentator import Instrumentator
from .models import Booking
from .connection import get_db_connection, create_booking_table

# Call the function to create the table
create_booking_table()

app = FastAPI()

# Create the Prometheus metrics
booking_created_total = Counter(
    'booking_created_total',
    'Total number of booking creations',
)

booking_updated_total = Counter(
    'booking_updated_total',
    'Total number of booking updates',
)

booking_deleted_total = Counter(
    'booking_deleted_total',
    'Total number of booking deletions',
)

# Instrument the app with Prometheus middleware
Instrumentator().instrument(app).expose(app)

# Routes
@app.get("/")
def read_root():
    return {"message": "Welcome to the Booking Microservice!"}

@app.post("/bookings")
def create_booking(booking: Booking):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "INSERT INTO bookings (user_id, event_id) VALUES (%s, %s) RETURNING id",
        (booking.user_id, booking.event_id),
    )
    booking_id = cur.fetchone()[0]
    conn.commit()
    cur.close()
    conn.close()
    booking_created_total.inc()  # Increment the booking_created_total counter
    return {"message": "Booking created successfully!", "booking_id": booking_id}

@app.get("/bookings/{booking_id}")
def get_booking(booking_id: int):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "SELECT user_id, event_id FROM bookings WHERE id = %s",
        (booking_id,),
    )
    booking_data = cur.fetchone()
    cur.close()
    conn.close()

    if booking_data:
        booking = {
            "id": booking_id,
            "user_id": booking_data[0],
            "event_id": booking_data[1],
        }
        return {"booking": booking}
    else:
        raise HTTPException(status_code=404, detail="Booking not found")

@app.put("/bookings/{booking_id}")
def update_booking(booking_id: int, booking: Booking):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "UPDATE bookings SET user_id = %s, event_id = %s WHERE id = %s",
        (booking.user_id, booking.event_id, booking_id),
    )
    conn.commit()
    cur.close()
    conn.close()
    booking_updated_total.inc()  # Increment the booking_updated_total counter
    return {"message": "Booking updated successfully!"}

@app.delete("/bookings/{booking_id}")
def delete_booking(booking_id: int):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("DELETE FROM bookings WHERE id = %s", (booking_id,))
    conn.commit()
    cur.close()
    conn.close()
    booking_deleted_total.inc()  # Increment the booking_deleted_total counter
    return {"message": "Booking deleted successfully!"}

@app.get("/users/{user_id}/bookings")
def get_user_bookings(user_id: int):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "SELECT id, user_id, event_id FROM bookings WHERE user_id = %s",
        (user_id,),
    )
    bookings = []
    for booking_data in cur.fetchall():
        booking = {
            "id": booking_data[0],
            "user_id": booking_data[1],
            "event_id": booking_data[2],
        }
        bookings.append(booking)
    cur.close()
    conn.close()
    return {"bookings": bookings}

@app.get("/events/{event_id}/bookings")
def get_event_bookings(event_id: int):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        "SELECT id, user_id, event_id FROM bookings WHERE event_id = %s",
        (event_id,),
    )
    bookings = []
    for booking_data in cur.fetchall():
        booking = {
            "id": booking_data[0],
            "user_id": booking_data[1],
            "event_id": booking_data[2],
        }
        bookings.append(booking)
    cur.close()
    conn.close()
    return {"bookings": bookings}

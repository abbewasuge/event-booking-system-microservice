
## Event Booking Management system
This project is a  system that facilitates user management, booking management, and event management. It provides a  platform for users to register, manage their bookings, and participate in various events. The system is designed to simplify the process of organizing and attending events, while ensuring a smooth user experience.


## Api Endpoints

The following API endpoints are available in the Project:

## User Management
- **GET /**: Retrieves the root endpoint and returns a welcome message.
- **POST /users**: Creates a new user by accepting user data in the request body.
- **GET /users**: Retrieves a list of all users from the database.
- **GET /users/{user_id}**: Retrieves a specific user by their user ID.
- **PUT /users/{user_id}**: Updates a specific user by their user ID, with the updated user data provided in the request body.
- **DELETE /users/{user_id}**: Deletes a specific user by their user ID.

## Prometheus Metrics User

- **user_created_total**: Tracks the total number of user creations.
- **user_updated_total**: Tracks the total number of user updates.
- **user_deleted_total**: Tracks the total number of user deletions.
- **http_requests_total**: Tracks the total number of HTTP requests made to each endpoint. It includes labels for the HTTP method and endpoint path.

## Event Management

- **POST /events**: Create a new event.
- **GET /events**: Get a list of all events.
- **GET /events/{event_id}**: Retrieve event details by event ID.
- **PUT /events/{event_id}**: Update event details by event ID.
- **DELETE /api/events/{event_id}**: Delete an event by event ID.
- **GET /events/date_range**: Get events within a specified date range.
- **GET /users/{user_id}/events**: Get events attended by a specific user.
- **POST /events/{event_id}/attendees**: Add an attendee to an event.

## Prometheus Metrics Event

- **event_created_total**: Total number of event creations.
- **event_updated_total**: Total number of event updates.
- **event_deleted_total**: Total number of event deletions.
- **http_requests_total**: Total number of HTTP requests, categorized by method and endpoint.


## Booking Management

- **POST /bookings**: Create a new booking.
- **GET /bookings/{booking_id}**: Retrieve booking details by booking ID.
- **PUT /bookings/{booking_id}**: Update booking details by booking ID.
- **DELETE /bookings/{booking_id}**: Delete a booking by booking ID.
- **GET /users/{user_id}/bookings**: Get bookings made by a specific user.
- **GET /events/{event_id}/bookings**: Get bookings associated with a specific event.

## Prometheus Metrics Booking

- **booking_created_total**: Total number of booking creations.
- **booking_updated_total**: Total number of booking updates.
- **booking_deleted_total**: Total number of booking deletions.


## Project Setup

**Prerequisites**
Before you proceed with the setup, make sure you have the following prerequisites installed on your system:

- **Docker**: Install Docker
- **Docker Compose**: Install Docker Compose

```
docker-compose up
```
Once the services are up and running, you can access them as follows:

    User Management: http://localhost:8000
    Booking Management: http://localhost:8001
    Event Management: http://localhost:8002